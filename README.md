# Group Name

One Man Solo, No Regret

## Group Member

Somruk Laothamjinda 602115023

## Email Address

602115023 - somruk_laothamjinda@cmu.ac.th / somruk_laothamjinda@elearning.cmu.ac.th / somruk99@hotmail.com (Just use the first email address)

## Webpage URL

http://52.23.182.15:8085/

## Backend Port/IP

Port - 8086
IP   - 52.23.182.15
Port/IP - 52.23.182.15:8086

## Katalon Test Repository Link

https://gitlab.com/se234-602115023/234project5023-systemtest

