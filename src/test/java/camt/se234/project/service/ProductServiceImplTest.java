package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ProductServiceImplTest {

    ProductDao productDao;
    ProductServiceImpl productService;

    static List<Product> mockProductList;

    @BeforeClass
    public static void setupTestData() {
        mockProductList = new ArrayList<Product>();
        mockProductList.add(Product.builder().id(444L).productId("p4444").name("Full House DVD Season 1 DVD Set").price(1500.00).description("A complete season 1 of Full House series").imageLocation(null).build());
        mockProductList.add(Product.builder().id(888L).productId("p2030").name("A Fragment of 2030").price(2030.00).description("The best Craft Essence in F/GO, trust me").imageLocation(null).build());
        mockProductList.add(Product.builder().id(416L).productId("p0416").name("M416 Tactical").price(23500.50).description("Best gun from the country with the best engineering").imageLocation(null).build());
        mockProductList.add(Product.builder().id(404L).productId("p0404").name("Error Product").price(-404.04).description("404 Not found").imageLocation(null).build());
        mockProductList.add(Product.builder().id(431L).productId("p3310").name("Nokia 3310").price(-99999.99).description("The weapon to surpass Metal Gear").imageLocation(null).build());
    }

    @Before
    public void setupDaoService() {
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
        when(productDao.getProducts()).thenReturn(mockProductList);
    }

    @Test
    public void testGetAllProducts() {
        //contains do not work well with ArrayList so converting to Array is needed
        assertThat(productService.getAllProducts(),contains(mockProductList.toArray()));
    }

    @Test
    public void testGetAvailableProducts() {
        assertThat(productService.getAvailableProducts(),containsInAnyOrder(
                Product.builder().id(444L).productId("p4444").name("Full House DVD Season 1 DVD Set").price(1500.00).description("A complete season 1 of Full House series").imageLocation(null).build(),
                Product.builder().id(888L).productId("p2030").name("A Fragment of 2030").price(2030.00).description("The best Craft Essence in F/GO, trust me").imageLocation(null).build(),
                Product.builder().id(416L).productId("p0416").name("M416 Tactical").price(23500.50).description("Best gun from the country with the best engineering").imageLocation(null).build()
        ));
    }

    @Test (expected = AssertionError.class)
    public void testGetAvailableProductsExpectError() {
        //contains do not work well with ArrayList so converting to Array is needed
        assertThat(productService.getAvailableProducts(),containsInAnyOrder(mockProductList.toArray()));
        assertThat(productService.getAvailableProducts(),hasItems(
                Product.builder().id(404L).productId("p0404").name("Error Product").price(-404.04).description("404 Not found").imageLocation(null).build(),
                Product.builder().id(431L).productId("p3310").name("Nokia 3310").price(-99999.99).description("The weapon to surpass Metal Gear").imageLocation(null).build()
        ));
    }

    @Test
    public void testGetUnavailableProductSize() {
        assertThat(productService.getUnavailableProductSize(),is(2));
    }



}
