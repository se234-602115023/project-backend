package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.dao.OrderDaoImpl;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import java.util.ArrayList;

import java.util.List;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;

import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class SaleOrderServiceImplTest {

    static List<SaleOrder> mockSaleOrderList;

    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;

    @BeforeClass
    public static void setupTestData() {
        //Create mock lists
        mockSaleOrderList = new ArrayList<SaleOrder>();
        List<SaleTransaction> mockSaleTransactionList = new ArrayList<SaleTransaction>();
        List<Product> mockProductList = new ArrayList<Product>();

        //Create all mock products
        mockProductList.add(
                Product.builder().id(444L).productId("p4444").name("Full House DVD Season 1 DVD Set").price(1500.00).description("A complete season 1 of Full House series").imageLocation(null).build()
        );
        mockProductList.add(
                Product.builder().id(888L).productId("p2030").name("A Fragment of 2030").price(2030.00).description("The best Craft Essence in F/GO, trust me").imageLocation(null).build()
        );
        mockProductList.add(
                Product.builder().id(416L).productId("p0416").name("M416 Tactical").price(23500.50).description("Best gun from the country with the best engineering").imageLocation(null).build()
        );
        mockProductList.add(
                Product.builder().id(676L).productId("p5023").name("Leng's Soul").price(50.23).description("A soul of someone who is dying from doing 5+ projects for final").imageLocation(null).build()
        );

        //Create all mock sale transactions
        mockSaleTransactionList.add(SaleTransaction.builder().id(001L).transactionId("t001").product(mockProductList.get(0)).amount(4).build());
        mockSaleTransactionList.add(SaleTransaction.builder().id(002L).transactionId("t002").product(mockProductList.get(1)).amount(8).build());
        mockSaleTransactionList.add(SaleTransaction.builder().id(003L).transactionId("t003").product(mockProductList.get(2)).amount(2).build());
        mockSaleTransactionList.add(SaleTransaction.builder().id(004L).transactionId("t004").product(mockProductList.get(3)).amount(23).build());

        //Create all mock sale orders
        mockSaleOrderList.add(SaleOrder.builder().id(234L).saleOrderId("s004").transactions(
                new ArrayList<SaleTransaction>() {{
                    add(mockSaleTransactionList.get(0));
                    add(mockSaleTransactionList.get(1));
                }}
        ).build());

        mockSaleOrderList.add(SaleOrder.builder().id(253L).saleOrderId("s005").transactions(
                new ArrayList<SaleTransaction>() {{
                    add(mockSaleTransactionList.get(2));
                }}
        ).build());

        mockSaleOrderList.add(SaleOrder.builder().id(134L).saleOrderId("s007").transactions(
                new ArrayList<SaleTransaction>() {{
                    add(mockSaleTransactionList.get(3));
                }}
        ).build());

    }

    @Before
    public void setupDaoService() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);
        when(orderDao.getOrders()).thenReturn(mockSaleOrderList);
    }

    @Test
    public void testGetSaleOrders() {
        assertThat(saleOrderService.getSaleOrders(),containsInAnyOrder(
                mockSaleOrderList.get(2),
                mockSaleOrderList.get(1),
                mockSaleOrderList.get(0)
        ));
        assertThat(saleOrderService.getSaleOrders(),containsInAnyOrder(
                mockSaleOrderList.get(1),
                mockSaleOrderList.get(2),
                mockSaleOrderList.get(0)
        ));
    }


    @Test (expected = AssertionError.class)
    public void testGetSaleOrdersExpectError() {
        assertThat(saleOrderService.getSaleOrders(),containsInAnyOrder(
                mockSaleOrderList.get(0),
                mockSaleOrderList.get(2)
        ));
        assertThat(saleOrderService.getSaleOrders(),containsInAnyOrder(
                mockSaleOrderList.get(2)
        ));
    }

    @Test
    public void testGetAverageSaleOrderPrice() {
        assertThat(saleOrderService.getAverageSaleOrderPrice(),closeTo(23465.43,0.1));
    }


    }

