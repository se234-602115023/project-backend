package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AuthenticationServiceImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticationService;

    static User expectedMockUser;
    static User expectedMockUser2;

    @BeforeClass
    public static void setupTestData() {
        expectedMockUser = User.builder().id(124L).username("gaben").password("morethan2lessthan4").role("admin").build();
        expectedMockUser2 = User.builder().id(444L).username("mista").password("guido").role("user").build();
    }

    @Before
    public void setupDaoService() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
        when(userDao.getUser("gaben","morethan2lessthan4")).thenReturn(expectedMockUser);
        when(userDao.getUser("mista","guido")).thenReturn(expectedMockUser2);
    }

    @Test
    public void testAuthenticateExistUser() {
        assertThat(authenticationService.authenticate("gaben","morethan2lessthan4"),is(expectedMockUser));
        assertThat(authenticationService.authenticate("mista","guido"),is(expectedMockUser2));
    }

    @Test
    public void testAuthenticateNonExistUser() {
        assertThat(authenticationService.authenticate("johnCena","ucantseeme"),nullValue());
        assertThat(authenticationService.authenticate("password","username"),nullValue());
    }

}
